
import math as m

from vector import Vec4
from particle import Particle

class Algorithm:

    def Yij(self,p,q):
        pq = p.px*q.px+p.py*q.py+p.pz*q.pz
        return 2.*pow(min(p.E,q.E),2)*(1.0-min(max(pq/m.sqrt(p.P2()*q.P2()),-1.0),1.0))/self.ecm2

    def Cluster(self,event):
        self.ecm2 = (event[0].mom+event[1].mom).M2()
        p = [ i.mom for i in event[2:] ]
        kt2 = []
        n = len(p)
        imap = range(n)
        kt2ij = [ [ 0 for i in range(n) ] for j in range(n) ]
        dmin = 1
        for i in range(n):
            for j in range(i):
                dij = kt2ij[i][j] = self.Yij(p[i],p[j])
                if dij < dmin: dmin = dij; ii = i; jj = j
        while n>2:
            n -= 1
            kt2.append(dmin)
            jjx = imap[jj]
            p[jjx] += p[imap[ii]]
            for i in range(ii,n): imap[i] = imap[i+1]
            for j in range(jj): kt2ij[jjx][imap[j]] = self.Yij(p[jjx],p[imap[j]])
            for i in range(jj+1,n): kt2ij[imap[i]][jjx] = self.Yij(p[jjx],p[imap[i]])
            dmin = 1
            for i in range(n):
                for j in range(i):
                    dij = kt2ij[imap[i]][imap[j]]
                    if dij < dmin: dmin = dij; ii = i; jj = j
        return kt2

from histogram import Histo1D

class Analysis:

    def __init__(self,n=1):
        self.n = 0.
        self.lnt = Histo1D(100,0.0,9.0,'/LL_JetRates/ln_t\n')
        self.duralg = Algorithm()

    def Analyze(self,event,w,ts,weights=[]):
        n_splittings = len(event) - 4
        if n_splittings == 2: 
            nb = 0
            for e in event[4:]: 
                if abs(e.pid) == 5:
                    nb += 1
            if nb >= 2:
                self.n += 1.
                self.lnt.Fill(m.log(ts[1]),w)

    def Finalize(self,name):
        # self.lnt.ScaleW(1./self.n)
        file = open(name+".yoda","w")
        file.write(str(self.lnt))
        file.close() 
