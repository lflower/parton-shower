import math as m
import random as r
from vector import Vec4 
from particle import Particle, CheckEvent
from qcd import AlphaS, NC, TR, CA, CF

class Kernel:
    def __init__(self,flavs):
        self.flavs = flavs

class Pqq (Kernel):
    # quark -> quark (p_i) + gluon (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)
    
    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return 1

    def Value(self,z,y,t):
        # eq. 3.22a in arXiv:1411.4085 
        return self.Weight(t) * CF*(2./(1.-z*(1.-y))-(1.+z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * CF*2./(1.-z)

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * CF*2.*m.log((1.-zm)/(1.-zp))

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return 1.+(zp-1.)*m.pow((1.-zm)/(1.-zp),r.random())

class Pgg1 (Kernel):
    # gluon -> gluon (p_i) + gluon (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)

    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return 1

    def Value(self,z,y,t):
        # eq. 3.22b in arXiv:1411.4085 
        return self.Weight(t) * 2*CA*(1/(1.-z*(1.-y)) - 1. + z*(1.-z)/2.)

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * 2*CA*1/(1.-z)

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * 2*CA*m.log((1-zm)/(1-zp))

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return 1.+(zp-1.)*m.pow((1.-zm)/(1.-zp),r.random()) 

class Pgg2 (Kernel):
    # gluon -> gluon (p_i) + gluon (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)

    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return 1

    def Value(self,z,y,t):
        # eq. 3.22b in arXiv:1411.4085 
        return self.Weight(t) * 2*CA*(1/(y+z*(1-y)) - 1. + z*(1.-z)/2.)

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * 2*CA*1./z

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * 2*CA*m.log(zp/zm)

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        # m_zmin*pow(m_zmax/m_zmin,ATOOLS::ran->Get());
        return zm*m.pow(zp/zm,r.random()) 

class Pgq (Kernel):
    # gluon -> quark (p_i) + antiquark (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)
    
    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return 1

    def Value(self,z,y,t):
        # eq. 1.3c
        return self.Weight(t) * TR*(z*z + (1-z)*(1-z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * TR

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * TR * (zp-zm)

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return zm+(zp-zm)*r.random()


class Shower:

    def __init__(self,alpha,t0,matching="shower",mur2fac=1.):
        self.t0 = t0 # cutoff scale
        self.alpha = alpha # strong coupling 
        self.alphamax = alpha(self.t0) # alpha at minimal scale 
        self.mur2facs = [1./mur2fac,mur2fac] if mur2fac != 1. else []
        self.matching = matching 

        # all relevant quark flavours 
        self.kernels = [ Pqq([fl,fl,21]) for fl in [-5,-4,-3,-2,-1,1,2,3,4,5] ]
        self.kernels += [ Pgq([21,fl,-fl]) for fl in [1,2,3,4,5] ]
        self.kernels += [ Pgg1([21,21,21]) ]
        self.kernels += [ Pgg2([21,21,21]) ]

    def MECorrection(self,z,y):
        x = (1.-z)*(1.-y)
        num = (1.-x)*(pow(1.-x,2)+pow(x/(1.-z),2))
        den = 1.-x*(1.+z)+x*x*(z+pow(z/(1.-z),2))
        return num/den

    def MakeKinematics(self,z,y,phi,pijt,pkt):
        # pijt=p_ij(t) is the emitter momentum before branching 
        # pkt=p_k(t) is the spectator momentum before branching 
        # rkt^2 = 2 pijt.pkt y z (1-z)
        Q = pijt + pkt # total momentum before splitting 
        rkt = m.sqrt(Q.M2() *y*z*(1.-z)) # magnitude of transverse momentum 
                                        #(if emitter and spectator are massless)
        kt1 = pijt.Cross(pkt) # a transverse momentum 
        if kt1.P() < 1.e-6: # if pij and pk are collinear 
            kt1 = pijt.Cross(Vec4(0.,1.,0.,0.)) # perpendicular to x direction 
        kt1 *= rkt * m.cos(phi) / kt1.P() # correct magnitude, add random phase 

        kt2cms = Q.Boost(pijt).Cross(kt1)
        kt2cms *= rkt * m.sin(phi) / kt2cms.P() 
        kt2 = Q.BoostBack(kt2cms)

        pi = z*pijt + (1.-z)*y*pkt + kt1 + kt2 
        pj = (1.-z)*pijt + z*y*pkt - kt1 - kt2 
        pk = (1.-y)*pkt 

        return [pi,pj,pk]

    def MakeColors(self,flavs,colij,colk):
        self.c += 1 # will be set in Run() 
        if flavs[0] != 21:
            if flavs[1] != 21:
                if flavs[0] > 0:
                    return [ [self.c,0], [colij[0],self.c] ]
                else:
                    return [ [0,self.c], [self.c,colij[1]] ]
            else:
                if flavs[0] > 0:
                    return [ [colij[0],self.c], [self.c,0] ]
                else:
                    return [ [self.c,colij[1]], [0,self.c] ]
        else:
            if flavs[1] == 21:
                if colij[0] == colk[1]:
                    if colij[1] == colk[0] and r.random()>0.5:
                        return [ [colij[0],self.c], [self.c,colij[1]] ]
                    return [ [self.c,colij[1]], [colij[0],self.c] ]
                else:
                    return [ [colij[0],self.c], [self.c,colij[1]] ]
            else:
                if flavs[1] > 0:
                    return [ [colij[0],0], [0,colij[1]] ]
                else:
                    return [ [0,colij[1]], [colij[0],0] ]

    def Reweight(self,parton,t,g,h,accept):
        for i in range(len(self.mur2facs)):
            f = g*self.alpha(self.mur2facs[i]*t)/self.alpha(t)
            parton.AddWeight(i,t,f/g if accept else (h-f)/(h-g))

    def AddWeight(self,event,t):
        for parton in event:
            parton.GetWeight(self.w,t)
            parton.wgt = []

    def GeneratePoint(self,event):
        while self.t > self.t0 and self.nsplittings < maxem:
            t = self.t0 # comparing value
            # iterate over all splitter-spectator pairs that are colour-connected
            for split in event[2:]:
                for spect in event[2:]:
                    if spect == split:
                        continue # move on to next item 
                    if not split.ColorConnected(spect):
                        continue 
                    # iterate over all kernels that can branch the splitter 
                    for sf in self.kernels: 
                        if sf.flavs[0] != split.pid:
                            continue 

                        class_name = sf.__class__.__name__
                        if class_name == "Pgg1":
                            if split.ColorAndAntiColorConnected(spect) == (True,False):
                                continue 
                        elif class_name == "Pgg2":
                            if split.ColorAndAntiColorConnected(spect) == (False,True):
                                continue 
                        # compute z boundaries 
                        # t = |k_T|^2
                        # min and max z are roots of this eqn 
                        m2 = (split.mom + spect.mom).M2()
                        if m2 < 4*self.t0:
                            continue # no allowed z 
                        zp = 0.5*(1+m.sqrt(1-4*self.t0/m2))
                        # trial emission, overestimating alpha and using splitting function overestimate 
                        g = self.alphamax/(2*m.pi) * sf.Integral(1.-zp,zp,self.t) 
                        tt = self.t * m.pow(r.random(),1./g)
                        # take the higher ordering variable and store data 
                        if tt > t:
                            t = tt 
                            s = [split, spect, sf, m2, zp]
            self.t = t
            # if 'winner' was found, generate a value for z 
            if t > self.t0:
                z = s[2].GenerateZ(1.-s[4],s[4])
                # calculate y from z and t
                y = t/s[3]/z/(1.-z)
                if y < 1: 
                    # accept/reject procedure: compare f(z)/g(z) to a random number 
                    # extra factor 1-y is the phase-space factor. eq. 5.20
                    # f/g is the same regardless of weight 
                    # h/g = weight 
                    f = (1.-y) * self.alpha(t) * s[2].Value(z,y,t) / s[2].Weight(t)
                    g = self.alphamax * s[2].Estimate(z,t) / s[2].Weight(t)
                    h = self.alphamax * s[2].Estimate(z,t)

                    if matching == "powheg":
                        if len(event)==4: # POWHEG ONLY 
                            f *= self.MECorrection(z,y) # what if f becomes larger than g? 

                    if f >= g: print("uh oh")

                    if f/g > r.random():
                        # splitting happened!
                        self.nsplittings += 1
                        
                        phi = 2*m.pi*r.random() # random azimuthal angle 
                        moms = self.MakeKinematics(z,y,phi,s[0].mom,s[1].mom)
                        cols = self.MakeColors(s[2].flavs,s[0].col,s[1].col)
                        event.append(Particle(s[2].flavs[2],moms[1],cols[1])) 
                        # splitter and spectator get new kinematics, flavour and colour 
                        s[0].Set(s[2].flavs[1],moms[0],cols[0])
                        s[1].mom = moms[2]

                        self.eventweight *= g/h # analytic reweighting
                        self.Reweight(s[0],t,f,g,1)
                        return 
                    else: 
                        self.eventweight *= g/h * (h-f)/(g-f) # analytic reweighting 
                        self.Reweight(s[0],t,f,g,0)

    def Run(self,event,weight,t):
        self.nsplittings = 0
        self.c = 1
        self.t = t
        self.eventweight = weight 
        self.w = [ 1. for mur2fac in self.mur2facs ]
        while self.t > self.t0 and self.nsplittings < maxem:
            self.GeneratePoint(event)
            self.AddWeight(event,self.t)
        self.AddWeight(event,self.t0)

# test program 
# build and run the generator

import sys
from matrix import eetojj
from durham import Analysis

matching = "shower"
# matching = "powheg"
# matching = "mcnlo"

maxem = 200

alphas = AlphaS(91.1876,0.118) # set running coupling: alpha_s(Mz)=0.118
hardxs = eetojj(alphas)
shower = Shower(alphas,t0=1.,matching=matching,mur2fac=1.) # cutoff scale = 1 GeV
jetrat = Analysis(len(shower.mur2facs))

r.seed(123456)
nevents = 10000
for i in range(nevents): # no of events generated 
    if matching == "powheg":
        event, weight = hardxs.GeneratePOWHEGPoint()
    elif matching == "mcnlo":
        event, weight = hardxs.GenerateMCNLOPoint()
    else: 
        event, weight = hardxs.GenerateLOPoint()

    t = (event[0].mom+event[1].mom).M2()

    if len(event) > 4: # automatically MC@NLO only, hard events 
        Dijk = hardxs.RSub(event[3].pid,event[0].mom,
        event[3].mom,event[4].mom,
        event[2].mom,91.1876)
        s12 = event[3].mom*event[4].mom
        s13 = event[3].mom*event[2].mom
        s23 = event[4].mom*event[2].mom
        if Dijk[0]/(Dijk[0]+Dijk[1]) > r.random():
            z1 = s12/(s12+s23)
            t = s13*z1*(1.-z1)
        else:
            z2 = s12/(s12+s13)
            t = s23*z2*(1.-z2)

    shower.Run(event,weight,t)
    finalweight = shower.eventweight
    sys.stdout.write('\rEvent {0}'.format(i))
    sys.stdout.flush()
    jetrat.Analyze(event,finalweight,shower.w) # Durham jet algorithm analysis 
jetrat.Finalize("qcd{}_{}".format(matching,nevents))
print("")
