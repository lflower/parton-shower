
import math as m

from vector import Vec4
from particle import Particle

class Algorithm:

    def Yij(self,p,q):
        pq = p.px*q.px+p.py*q.py+p.pz*q.pz
        return 2.*pow(min(p.E,q.E),2)*(1.0-min(max(pq/m.sqrt(p.P2()*q.P2()),-1.0),1.0))/self.ecm2

    def lepton_number(self,particle):
        id = particle.pid 
        if id > 0:
            if id == 11 or id == 13 or id == 15:
                return 1 
            else: 
                return 0
        else: 
            if id == -11 or id == -13 or id == -15:
                return -1 
            else: 
                return 0

    def Cluster(self,event):
        self.ecm2 = (event[0].mom+event[1].mom).M2()
        p = [ i.mom for i in event[2:] ]
        kt2 = []
        n = len(p)
        kt2ij = [ [ 0 for i in range(n) ] for j in range(n) ]
        dmax = 0.001
        n_leptons = 0
        for p in event[2:]:
            if self.lepton_number(p) == 1:
                n_leptons += 1
        return n_leptons 
        

from histogram import Histo1D

class Analysis:

    def __init__(self,n=1):
        self.n = 0.
        self.hist = Histo1D(4,0.,4.,'/LL_LeptonRates/n_leptons\n')
        self.duralg = Algorithm()

    def Analyze(self,event,w,weights=[]):
        self.n += 1.
        nleptons = self.duralg.Cluster(event)
        self.hist.Fill(nleptons,w)

    def Finalize(self,name):
        self.hist.ScaleW(1./self.n)
        file = open(name+".yoda","w")
        file.write(str(self.hist))
        file.close() 
