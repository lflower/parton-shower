
import math as m

from vector import Vec4
from particle import Particle

class Planes:

    def lepton_number(self,particle):
        id = particle.pid 
        if id > 0:
            if id == 11 or id == 13 or id == 15:
                return 1 
            else: 
                return 0
        else: 
            if id == -11 or id == -13 or id == -15:
                return -1 
            else: 
                return 0

    def normal(self,p,q):
        # p and q are 4-vectors
        x = p.Cross(q)
        return x/m.sqrt(-x.M2())

    def costheta(self,p,q):
        # p and q are lists of pairs of momenta with the same flavour 
        p1 = p[0]; p2 = p[1]; pa = q[0]; pb = q[1] 
        norm1 = self.normal(p1,p2)
        norm2 = self.normal(pa,pb)
        # norm1 and norm2 are 4-vectors with 0-component = 0 
        dotprod = norm1.px*norm2.px+norm1.py*norm2.py+norm1.pz*norm2.pz
        mag1 = m.sqrt(-norm1.M2())
        mag2 = m.sqrt(-norm2.M2())
        return dotprod/(mag1*mag2)

    def Cluster(self,event):
        # flavour matching 
        momlist = []
        for i,e in enumerate(event[2:]):
            for f in event[2+i+1:]:
                if e.pid == -f.pid:
                    momlist.append([e.mom,f.mom])

        return self.costheta(momlist[0],momlist[1])

from histogram import Histo1D

class Analysis:

    def __init__(self,n=1):
        self.n = 0.
        self.hist = Histo1D(100,-1.,1.,'/LL_PlaneAngle/cos_theta\n')
        self.planealg = Planes()

    def Analyze(self,event,w,weights=[]):
        # check for 4 fermions in FS 
        nf = 0
        for e in event[2:]: 
            if e.pid != 21 and e.pid != 22:
                nf += 1
        if nf == 4:
            costheta = self.planealg.Cluster(event)
            self.n += 1.
            self.hist.Fill(costheta,w)

    def Finalize(self,name):
        self.hist.ScaleW(1./self.n)
        file = open(name+".yoda","w")
        file.write(str(self.hist))
        file.close() 
